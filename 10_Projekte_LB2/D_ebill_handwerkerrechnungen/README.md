# M122 - Aufgabe

2023-02 MUH


## ebill von Handwerkerrechnungen


### Ausgangslage ("Use Case")

Sie arbeiten in einer Firma, die sich zum Ziel gesetzt hat, 
für ihre Kunden und auch um neue Kunden zu gewinnen, die 
Rechnungen, die die Handwerker für deren Kunden erstelle,
den elektronischen Zahlungsweg anzubieten.

Der elektronische Zahlungsweg wird nicht über das Verschicken
einer PDF-Rechnung gemacht, wo der Kunde die Rechnung über das
Eintippen der Zahlungsdaten im eBanking machen muss, sondern als
 [**ebill**](https://ebill.ch). Also als eine Rechnung, die dem Kunden
des Handwerkers direkt in sein eBanking hineingeschickt wird und 
von dort aus direkt mit zwei Klicks bezahlt werden kann.
 
**Resultat**

![x-ressourcen/example-qr-bill-de.six-image.original.1020.png](x-ressourcen/example-qr-bill-de.six-image.original.1020.png)

<hr>

### Aufgabenstellung

![ebill-uebersicht.jpg](x-ressourcen/ebill-uebersicht.jpg)


Sie bekommen auf dem System des Rechnungsstellers in einem 
eigenen Format direkt von den Kassensystemen Rechnungsdateien 
mit allen Informationen, die es für die Rechnungsstellung 
über e-Bill braucht. 

Sie holen diese per FTP ab und erstellen daraus 
- Zwischenresultat a.) Eine menschenlesbare Rechnung im Klartext (Zur Vereinfachung nur als Text-Format, produktiv wäre das ein PDF)
- Zwischenresultat b.) Eine maschinenlesbare Rechnung im XML-Format für das SIX-PaymentServices-System (Schweizer Marktleader für die Zahlungsabwicklung)

Diese beiden Dateien geben Sie per FTP in das «Zahlungssystem» ab.
Dann erfolgt, für Sie im Hintergrund die Weiterleitung 
ins Bankensystem. Ob das geklappt hat, wissen Sie erst, 
in dem das Zahlungssystem eine Quittungsdatei produziert 
hat und Ihnen zur Verfügung stellt.

Ihrem Kunden (dem Rechnungssteller =Biller) stellen Sie 
diese Quittungsdatei sowohl per E-Mail wie auch im 
Rechnungsstellungs-System in einem Archiv (.zip) zur 
Verfügung, damit er überprüfen kann, ob die Rechnung 
korrekt verschickt wurde.

**Biller-System**

Sie bekommen vom Biller-Sytem **mehrere** solche 
Dateien


Dateiname:	rechnung23003.data

	Rechnung_23003;Auftrag_A003;Zürich;21.07.2023;10:22:54;ZahlungszielInTagen_30
	Herkunft;41010000001234567;K821;Adam Adler;Bahnhofstrasse 1;8000 Zuerich;CHE-111.222.333 MWST;harald.mueller@tbz.ch
	Endkunde;41301000000012497;Autoleasing AG;Gewerbestrasse 100;5000 Aarau
	RechnPos;1;Einrichten E-Mailclients;5;25.00;125.00;MWST_0.00%
	RechnPos;2;Konfig & Schulung Scanningcenter;1;1000.00;1000.00;MWST_0.00%


Sie müssen aus jeder Rechnungsdatei (.data) zwei Dateien erstellen
(ein TXT-File und ein XML-File) und diese müssen einen bestimmten Namen haben:
<br>**[Kundennummer]_[Rechnungsnummer]_invoice.xml** (also konkret ‘K821_23003_invoice.xml’)
<br>**[Kundennummer]_[Rechnungsnummer]_invoice.txt** (also konkret ‘K821_23003_invoice.txt’)

Die Dateinamen werden aus den **Inhalten** der .data-Datei gebildet (generiert): 
<br>(aus **Kundennummer** "K821" und aus der **Rechnungsnummer** "23003")

<br>Dateiname Zwischenresultat a.):	K821_23003_invoice.txt
<br>Dateiname Zwischenresultat b.):	K821_23003_invoice.xml


### Zwischenresultat a.) Rechnung in menschenlesbaren Klartext

Eine solche Datei wird den Endkundenen mitgeschickt, damit
überprüft werden kann, was zu bezahlen ist. Im eBanking-System 
haben Endkunden die Möglichkeit, die Rechnung zurückzuweisen 
und somit nicht zu bezahlen. 


**Inhalt von K821_23003_invoice.txt:**

	-------------------------------------------------
	
	
	
	Adam Adler
	Bahnhofstrasse 1
	8000 Zuerich
	
	CHE-111.222.333 MWST
	
	
	
	
	Uster, den 21.07.2023                           Autoleasing AG
	                                                Gewerbestrasse 100
	                                                5000 Aarau
	
	Kundennummer:      K821
	Auftragsnummer:    A003
		
	Rechnung Nr       23003
	-----------------------
	1   Einrichten E-Mailclients              5      25.00  CHF      125.00
	2   Konfig & Schulung Scanningcenter      1    1200.00  CHF     1200.00
                                                                 -----------       
                                                      Total CHF     1325.00

                                                      MWST  CHF        0.00
	
	
	
	
	
	
	
	
	Zahlungsziel ohne Abzug 30 Tage (20.08.2023)
	
	
	Empfangsschein             Zahlteil                
	
	Adam Adler                 ------------------------  Adam Adler       
	Bahnhofstrasse 1           |  QR-CODE             |  Bahnhofstrasse 1 
	8000 Zuerich               |                      |  8000 Zuerich     
	                           |                      |  
	                           |                      |  
	00 00000 00000 00000 00000 |                      |  00 00000 00000 00000 00000
	                           |                      |  
	Autoleasing AG             |                      |  Autoleasing AG
	Gewerbestrasse 100         |                      |  Gewerbestrasse 100
	5000 Aarau                 |                      |  5000 Aarau
	                           ------------------------
	Währung  Betrag            Währung  Betrag
	CHF      1325.00           CHF      1325.00								

	-------------------------------------------------


#### Vereinfachungen zum Profi-Umfeld

- 1.) Normalerweise sind diese Rechnungen hier in PDF erstellt. 
Damit es weniger Aufwand gibt, wird hier darauf verzichtet, aus 
dem Text ein PDF zu erzeugen. Die Einrückungen bräuchten eigentlich 
Tabulatoren. Das kann man mit dem "reinen Textformat" nicht machen. 
Stattdessen wird mit Leerzeichen eingerückt (vorne auffüllen).

- 2.) Die Erzeugung des QR-Codes im Einzahlungsschein-Bereich 
erfordert den Aufruf einer API-Funktion, die dann eine Grafik erzeugt 
und die dann in ein PDF integriert werden müsste. Auch darauf wird verzichtet. 
Stattdessen zeichnen wir einfach einen Rahmen mit Strich-Zeichen und 
schreiben das Wort "QR-CODE" hinein. 

- 3.) Auch auf die Auszeichnung der Texte wird auch verzichtet. 
(verschiedene Schriftgrössen, Fettschrift, Titelschrift, Logos)

- 4.) Es gibt Anwendungen wo Rechnungen unter Firmen (B2B = Business-to-Business) 
direkt verschickt werden. Dort könnte das Aufzählen der Rechnungspositionen (Artikel)
in der .xml-Datei auch nötig werden. Für "eBill" bei SIX sind wir im Bereich 
B2C (Business-to-Customer) und da ist das detaillierte Aufzählen nicht verlangt 
und wir werden darauf auch verzichten.



### Zwischenresultat b.) Rechnung im (maschinenlesbaren) XML-Format

**Inhalt von K821_23003_invoice.xml:**

	-------------------------------------------------
	<XML-FSCM-INVOICE-2003A>
	<INTERCHANGE>
		<IC-SENDER>
		<Pid>41010000001234567</Pid>
		</IC-SENDER>
		<IC-RECEIVER>
		<Pid>41301000000012497</Pid>
		</IC-RECEIVER>
		<IR-Ref />
	</INTERCHANGE>
	<INVOICE>
		<HEADER>
		<FUNCTION-FLAGS>
			<Confirmation-Flag />
			<Canellation-Flag />
		</FUNCTION-FLAGS>
		<MESSAGE-REFERENCE>
			<REFERENCE-DATE>
			<Reference-No>202307314522001</Reference-No>
			<Date>20230731</Date>
			</REFERENCE-DATE>
		</MESSAGE-REFERENCE>
		<PRINT-DATE>
			<Date>20230731</Date>
		</PRINT-DATE>
		<REFERENCE>
			<INVOICE-REFERENCE>
			<REFERENCE-DATE>
				<Reference-No>23003</Reference-No>
				<Date>20230731</Date>
			</REFERENCE-DATE>
			</INVOICE-REFERENCE>
			<ORDER>
			<REFERENCE-DATE>
				<Reference-No>A003</Reference-No>
				<Date>20230731</Date>
			</REFERENCE-DATE>
			</ORDER>
			<REMINDER Which="MAH">
			<REFERENCE-DATE>
				<Reference-No></Reference-No>
				<Date></Date>
			</REFERENCE-DATE>
			</REMINDER>
			<OTHER-REFERENCE Type="ADE">
			<REFERENCE-DATE>
				<Reference-No>202307164522001</Reference-No>
				<Date>20230731</Date>
			</REFERENCE-DATE>
			</OTHER-REFERENCE>
		</REFERENCE>
		<BILLER>
			<Tax-No>CHE-111.222.333 MWST</Tax-No>
			<Doc-Reference Type="ESR-ALT "></Doc-Reference>
			<PARTY-ID>
			<Pid>41010000001234567</Pid>
			</PARTY-ID>
			<NAME-ADDRESS Format="COM">
			<NAME>
				<Line-35>Adam Adler</Line-35>
				<Line-35>Bahnhofstrasse 1</Line-35>
				<Line-35>8000 Zürich</Line-35>
				<Line-35></Line-35>
				<Line-35></Line-35>
			</NAME>
			<STREET>
				<Line-35></Line-35>
				<Line-35></Line-35>
				<Line-35></Line-35>
			</STREET>
			<City></City>
			<State></State>
			<Zip></Zip>
			<Country></Country>
			</NAME-ADDRESS>
			<BANK-INFO>
			<Acct-No></Acct-No>
			<Acct-Name></Acct-Name>
			<BankId Type="BCNr-nat" Country="CH">001996</BankId>
			</BANK-INFO>
		</BILLER>
		<PAYER>
			<PARTY-ID>
			<Pid>41301000000012497</Pid>
			</PARTY-ID>
			<NAME-ADDRESS Format="COM">
			<NAME>
				<Line-35>Autoleasing AG</Line-35>
				<Line-35>Gewerbestrasse 100</Line-35>
				<Line-35>5000 Aarau</Line-35>
				<Line-35></Line-35>
				<Line-35></Line-35>
			</NAME>
			<STREET>
				<Line-35></Line-35>
				<Line-35></Line-35>
				<Line-35></Line-35>
			</STREET>
			<City></City>
			<State></State>
			<Zip></Zip>
			<Country></Country>
			</NAME-ADDRESS>
		</PAYER>
		</HEADER>
		<LINE-ITEM />
		<SUMMARY>
		<INVOICE-AMOUNT>
			<Amount>0000135000</Amount>
		</INVOICE-AMOUNT>
		<VAT-AMOUNT>
			<Amount></Amount>
		</VAT-AMOUNT>
		<DEPOSIT-AMOUNT>
			<Amount></Amount>
			<REFERENCE-DATE>
				<Reference-No></Reference-No>
				<Date></Date>
			</REFERENCE-DATE>
		</DEPOSIT-AMOUNT>
		<EXTENDED-AMOUNT Type="79">
			<Amount></Amount>
		</EXTENDED-AMOUNT>
		<TAX>
			<TAX-BASIS>
			<Amount></Amount>
			</TAX-BASIS>
			<Rate Categorie="S">0</Rate>
			<Amount></Amount>
		</TAX>
		<PAYMENT-TERMS>
			<BASIC Payment-Type="ESR" Terms-Type="1">
			<TERMS>
				<Payment-Period Type="M" On-Or-After="1" Reference-Day="31">30</Payment-Period>
				<Date>20230830</Date>
			</TERMS>
			</BASIC>
			<DISCOUNT Terms-Type="22">
			<Discount-Percentage>0.0</Discount-Percentage>
			<TERMS>
				<Payment-Period Type="M" On-Or-After="1" Reference-Day="31"></Payment-Period>
				<Date></Date>
			</TERMS>
			<Back-Pack-Container Encode="Base64"> </Back-Pack-Container>
			</DISCOUNT>
		</PAYMENT-TERMS>
		</SUMMARY>
	</INVOICE>
	</XML-FSCM-INVOICE-2003A>
	-------------------------------------------------

<br>

### System-Zugänge

**Browser:** https://haraldmueller.ch/schoolerinvoices (Passwort: "tbz")
<br>https://haraldmueller.ch/schoolerinvoices/out (Passwort auf html-Seite: "tbz")
<br>https://haraldmueller.ch/schoolerinvoices/in 

**FTP-Zugang:**

Als Hilfe zum Überprüfen, was Sie mit dem Skript schicken oder empfangen, 
verwenden Sie am Besten einen FTP-Client wie "FileZilla". Speichern Sie dort 
im "Servermanager" die Einstellungen.
![FileZillaClient](x-ressourcen/filezillaclient.jpg)


	HOST: "ftp.haraldmueller.ch"
	USER: "schoolerinvoices"
	PASS: "Berufsschule8005!"
	PATH: "/in/[Klasse]/[IhrNachname]"
	PATH: "/out/[Klasse]/[IhrNachname]"



![ebill-uebersicht-details.jpg](x-ressourcen/ebill-uebersicht-details.jpg)


**Browser:** https://coinditorei.com/zahlungssystem (Passwort: "tbz")
<br>https://coinditorei.com/zahlungssystem/in
<br>https://coinditorei.com/zahlungssystem/out 
<br>
<br>Zur Abgabe der Rechnung als TXT und XML auf dem Abgabeserver (Zahlungssystem/in/[KlasseUndIhrNachname]) per FTP

**FTP-Zugang:**

	HOST: "ftp.coinditorei.com"
	USER: "zahlungssystem"
	PASS: "Berufsschule8005!"
	PATH: "/in/[KlasseUndIhrNachname]"
	PATH: "/out/[KlasseUndIhrNachname]"


**Kontrollen**

Bauen Sie Kontrollen ein, um festzustellen, ob «die» Rechnung richtig verarbeitet wurde, bzw. ob und wann und wie ein allfälliger Abbruch stattgefunden hat. Am einfachsten sind Log-Files.
Konfiguration
Gut ist, wenn das System ‘von aussen’ konfiguriert / eingestellt werden kann (separates Config-File).

		

## Bewertung und Note

**Funktionalität**

Der automatische Ablauf und die korrekten Resultate bestimmen die Note.

Die Bewertung geschieht über einen Live-Test mit von der Lehrperson erstellten Test-Dateien. Es gibt eine normale ‘richtiges’ Testdatei und eine Testdatei, die unmöglich zu verarbeiten ist. Dabei wird geschaut, ob das System korrekt läuft. Die korrekt Testdatei soll richtig verarbeitet werden. Die inkorrekte Testdatei darf nicht verarbeitet werden. Dies müssen Sie nachvollziehen können.

**Dokumentation**

Es wird keine Doku verlangt und ist auch nicht notwendig, da die 
Aufgabenstellung schon so detailliert ist. (In der Praxis werden Sie 
niemals so genaue Vorgaben vorfinden, und dann wird vielleicht eine
Dokumentation verlangt werden.)


### Benotung

| Note | Bedeutung |
|------| --------- |
|   6  | Komplette Verarbeitung der Vorgaben inklusive Log-Dateien zur Rekonstruktion des Ablaufes. Alle Abläufe sind automatisiert und können per Konfigurationsdatei beeinflusst werden (FTP-Server Einstellungen). Es sind diverse Sicherheitsmechanismen eingebaut (Fehlermeldungen mit aussagekräftigem und nützlichem Inhalt).     |
|   5  | Das System funktioniert aus Sicht des Kunden, wenn alles richtig definiert war (keine Fehler in der Rechnungsdatei oder wenn die Quittung nicht vom Zahlungssystem bereitgestellt wurde). |
|   4  | Viele Teile der Vorgabe wurden erreicht, aber es fehlt noch etwas damit alles automatisch funktioniert.                  |


### Detailberechnung der Note

| Punkte |      | Bezeichnung |
| ------ | ---- | ----------- |
|    2   |      | Filedownload (.data) via FTP (aus dem [Kundenserver]/out/XX21xMustermann) |
|        | [1]  | Ein Dateidownload funktioniert |
|        | [1]  | es können auch mehrere Dateien "gleichzeitig" verarbeitet werden |
|    5   |      | Lesen der Input-Datei (.data) |    
|        | [3]  | Aufspalten der Informationen |
|        | [1]  | Erkennen falscher Informationen |
|        | [1]  | Rückweisen falscher/inkorrekter Rechnung |
|    12  |      | Erstellung der _Invoice.txt |
|        | [1]  | Richtiger Filename (gem. definierter Vorgabe) |
|        | [4]  | Korrekte Darstellung und Formatierung der Rechnung mit Einrückung und Kollonierung der Rechnungzeilen |
|        | [3]  | Richtige Berechnung der End-Summe inkl. Darstellung (2-Nummen nach dem Dez-Punkt) |
|        | [2]  | Einrücken und Darstellung des Einzahlungsschein-Abschnitts (Beträge haben Abstand beim Dezimalpunkt) |
|        | [2]  | Richtige Berechnung und Position des Zahlungsziel-Datum (Rechnungsdatum + Zahlungsziel), -> nicht Verarbeitungsdatum!! |
|    6   |      | Erstellung der _Invoice.xml |
|        | [1]  | Richtiger Filename (gem. definierter Vorgabe) |
|        | [1]  | Rechnungsnummer eingesetzt |
|        | [2]  | Summe korrekt (ohne Punkt, mit führenden Nullen) |
|        | [2]  | Zahlungsziel & Zahlungszieldatum (Korr. Datum und Formattierung YYYYmmdd) |
|    2   |      | Fileupload (2 Files (.txt und .xml) pro Rechnung) |
|        | [2]  | via FTP (auf den zweiten Server nach[Zahlungsserver]/in/XX21xMustermann) |
|        | [-1] | Abzug 1P dafür, falls die fehlerhafte Rechnung auch noch da steht  |
|    3   |      | Zip- oder tar-File Erstellung |
|        | [2]  | Zip-/tar-File mit korrektem Inhalt und Dateinamen (2 Files) |
|        | [1]  | Fileupload via FTP (auf den [Kundenserver]/in/XX21xMustermann) |
|    5   |      | Mailing |
|        | [2]  | Mail-Versand (kommt an der richtigen Adresse "heute/jetzt" an (Mailadr im Input)) |
|        | [2]  | Mail-Text und Absender fehlerlos, den Anforderungen entsprechend |
|        | [1]  | Mail-Attachment (.zip/.tar) geschickt/vorhanden |
|    5   |      | Konfiguration und Projektdateiorganisation |
|        | [2]  | "gute" Struktur der Projektdateien, Verarbeitungsdaten nicht bei den Verarbeitungs-Skript(s) |
|        | [2]  | Log-File mit vernünftigen/aussagekräftigen Informationen, z.B. Erkennung von fehlerhafter Verarbeitung |
|        | [1]  | separate Konfigurationsdatei |
|    2   |      | Automatisierung |
|        | [2]  | Scheduler eingerichtet und funktioniert (Linux "crontab" oder Win "Aufgabenplaner") |
|**42P** |      | **Total** |



