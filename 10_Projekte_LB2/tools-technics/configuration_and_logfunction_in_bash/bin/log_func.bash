# Usage: log <LOGLEVEL> <MESSAGE>
# Loglevel is one of: W,I,D,E or Warning,Info,Debug,Error
#
if [ -z $LOGLEVEL ] ; then
  LOGLEVEL=W        #Set default loglevel
fi

# if LOGFILE is set log to logfile otherwise log to stderr
log(){
  if [ -n "$LOGFILE" ] ; then
    logit $* 2>>$LOGFILE
  else
    logit $*
  fi
}
logit(){
  DATE=`date '+%Y.%m.%d %H:%M:%S'`
  LEVEL=$1
  shift
  case $LEVEL in
    D*)
      case $LOGLEVEL in
        D*)
        echo "$DATE:DEBUG:$*" 1>&2
        ;;
      esac
      ;;
    I*)
      case $LOGLEVEL in
        D*|I*)
        echo "$DATE:INFO:$*" 1>&2
        ;;
      esac
      ;;
    W*)
      case $LOGLEVEL in
        D*|I*|W*)
        echo "$DATE:WARNING:$*" 1>&2
        ;;
      esac
      ;;

    E*)
      echo "$DATE:ERROR:$*" 1>&2
      ;;
    *)
      echo "$DATE:UNKNOWN:$*" 1>&2
      ;;
    esac
}

