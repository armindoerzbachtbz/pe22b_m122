# M122 - Aufgabe

2023-02 MUH


## Dateien und Verzeichnisse anlegen

Als Lehrer habe ich den Bedarf, für alle Lernenden einer Klasse 
dieselben Dateien zur Verfügung zu stellen. Sie können sich
auch vorstellen, Sie müssen für eine Anzahl ihrer Kollegen oder
Mitarbeiter einen Satz von gleichen Dateien bereitstellen.

In diesem Beispiel gibt mehrere Klassen. Jede Klasse bekommt ein Verzeichnis.

In diesem Klassenverzeichnis wird für jede Person ein weiteres
Verzeichnis angelegt. Das heisst, dass alle Lernenden ein eigenes Verzeichnis bekommen. Und in diesem Lernenden-Verzeichnis sollen
dann die gleichen Dateien zur Verfügung gestellt werden.

### Gesucht

Es soll nachher eine solche Struktur entstehen:

	./M122-AP22b
		Amherd
			Datei-1.txt
			Datei-2.docx
			Datei-3.pdf				
		Baume-Schneider
			Datei-1.txt
			Datei-2.docx
			Datei-3.pdf
		Berset
			Datei-1.txt
			Datei-2.docx
			Datei-3.pdf

		  usw.
		
		
### Gegeben

Es wird eine Liste mit allen Namen aller Lernenden geben. 
(bitte selber herstellen)

Diese Datei bekommt den Namen (die Abkürzung) der Klasse:

	./M122-AP22b.txt

Der Inhalt der M122-AP22b.txt Datei sieht dann zum Beispiel
so aus (bitte selber herstellen)

	Amherd
	Baume-Schneider
	Berset
	Cassis
	Keller-Sutter
	Parmelin
	Roesti

<hr>

### Ihre Aufgabe 

- Teil 1: **Erstellen Sie zuerst die "Gegeben"-Dateien**

- Teil 2: **Lösen Sie dann die "Gesucht"-Aufgabe**


**Allgemeines:**

Sie sollten grundsätzlich selber versuchen, Ihre eigene 
Lösung zu finden. Sie können sich gerne auch mit jemandem 
zusammenzutun um zusammen zu forschen, nachzudenken und um
die ersten Schritte gemeinsam zu machen. Aber jede:r Lernende
gibt die eigene Lösung ab. 

Sie haben in den letzten Wochen **bash** gelernt. Die Idee
ist, dass nun mit dieser Skriptsprache die Lösung erarbeiten.

Wenn Sie gefitzt sind, werden Sie vielleicht auch eine 
einfachere und schnellere Lösung in **bash** finden.


**_beachten Sie:_**

**Bash** hat nicht die gleichen Möglichkeiten wie PowerShell. 
Manches ist einfachen und manches ist nicht so einfach zu 
machen. Jede Skript-Programmiersprache hat ihr eigenen Vor- und Nachteile. 

Und was auch nicht zu vernachlässigen ist, ist dass **bash** auf
jedem Linux-System ohne etwas zu installieren schon verfügbar ist.
Sie können immer davon ausgehen, dass Sie Ihr Linux-Bash-Skript
sofort auf einem fremden System verwenden. Das ist in der 
Praxis ein **Riesen-Vorteil**!



### Mögliche Strategie und Lösung in PowerShell 

Damit Sie einmal einen Einblick bekommen, ist hier in der 
Folge die gleiche Aufgabe in **PowerShell** programmiert
worden. Da es in PowerShell andere Möglichkeiten gibt,
können Sie nicht alles gleich machen. Aber die Strategie,
was zu machen ist, können Sie vielleicht, zumindest teilweise,
übernehmen.

- Hilfestellung für Teil 1

Mögliche Lösung: [in Powershell](loesung-in-powershell/prepareTemplate.ps1)


- Hilfestellung für Teil 2


1.) Das Skript muss zuerst den Dateinamen 
(vor dem Punkt) lesen um damit das Grundverzeichnis 
(das der Klasse) anzulegen. Diesen verwendeten Namen sollte
gespeichert werde, weil er vermutlich wieder verwendet 
werden wird.

2.) Dann muss das Skript die Datei öffnen und alle Namenseinträge 
herauslesen. 

3.) Die Namen sind in einer Liste notiert. Die können gleich
als "array" eingelesen und nachher so verwendet werden. 

Es gibt Möglichkeiten, die oben beiden Schritte in einem
einzigen Befehl zu machen um gleichzeitig den Inhalt einer
Datei direkt einer Programm-Variablen zuzuweisen und um den 
nächsten Schritt gleich damit zu machen.

4.) Mit jedem (Namens-)Eintrag muss man erstens ein 
Verzeichnis erstellen und zweitens die Dateien aus dem 
"Template"-Verzeichnis hinein zu kopieren.

- [./loesung-in-powershell/einstieg-prepareFiles-01.ps1](./loesung-in-powershell/einstieg-prepareFiles-01.ps1)
- [./loesung-in-powershell/einstieg-prepareFiles-02.ps1](./loesung-in-powershell/einstieg-prepareFiles-02.ps1)
- [./loesung-in-powershell/einstieg-prepareFiles-03.ps1](./loesung-in-powershell/einstieg-prepareFiles-03.ps1)
- [./loesung-in-powershell/einstieg-prepareFiles-04.ps1](./loesung-in-powershell/einstieg-prepareFiles-04.ps1)
- [./loesung-in-powershell/einstieg-prepareFiles-05.ps1](./loesung-in-powershell/einstieg-prepareFiles-05.ps1)
- [./loesung-in-powershell/einstieg-prepareFiles-06.ps1](./loesung-in-powershell/einstieg-prepareFiles-06.ps1)
- [./loesung-in-powershell/einstieg-prepareFiles-07.ps1](./loesung-in-powershell/einstieg-prepareFiles-07.ps1)


Das fertige Skript: [./loesung-in-powershell/prepareFiles.ps1](./loesung-in-powershell/prepareFiles.ps1)


<hr>

## Bewertung

| Stufe | Beschreibung | Punkte |
|-------|--------------|--------|
|     1 | "Gegeben-Skript:" Erstellung Namensdatei                         |  1 |
|     2 | "Gegeben-Skript:" Erstellung der Dateien im Template-Verzeichnis |  2 |
|     3 | "Gesucht-Skript:" Klassenverzeichnis wird angelegt               |  1 |
|     4 | "Gesucht-Skript:" Lernendenverzeichnisse werden angelegt         |  2 |
|     5 | "Gesucht-Skript:" Lernendenverzeichnisse haben die Dateien drin  |  1 |
|     6 | "Gesucht-Skript:" Mehrere Klassen- und Lernenden-Verzeichnisse   |  1 |
| Total |                                                                 | **8**|


### Noten

| Note| Punkte    |
|-----|-----------|
| 6.0 | über 7.7  |
| 5.5 |  6.8-7.6  |
| 5.0 |  6.0-6.8  | 
| 4.5 |  5.2-6.0  |
| 4.0 |  4.4-5.2  |
| 3.5 |  3.2-4.4  |
| 1.0 |Nichtabgabe|
