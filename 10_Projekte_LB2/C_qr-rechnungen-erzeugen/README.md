# M122 - Aufgabe

2023-02 MUH

## QR-Rechnungen erzeugen

Die QR-Rechnung, die seit Juni 2020 in Umlauf ist, 
hat am 1. Oktober 2022 die Schweizer Einzahlungsscheine 
endgültig abgelöst. Seither ist es im Zahlungswesen Pflicht,
damit zu arbeiten. Das heisst, dass alle Rechnungssteller,
wie zum Beispiel Handwerker, ihre alten roten und 
rosafarbenen Einzahlungsscheine nicht mehr brauchen dürfen
und müssen nun die QR-Rechnungen verwenden.


[Zahlungsstandard in der Schweiz (SIX)](https://www.six-group.com/de/products-services/banking-services/payment-standardization/standards/qr-bill.html)


Für diese Aufgsbenstellung werden wir folgende Web-App verwenden

[**https://qr-rechnung.net**](https://qr-rechnung.net)


Schauen Sie sich hier bitte mal etwas um. Wir verwenden nachher den Use Case **Aus Tabelle generieren**. Sie müssen also nachher eine Tabelle aus Daten erzeugen lassen.

<br>

**Beispiel-Rechnung**

[<img src="https://www.einfach-zahlen.ch/content/dam/einfachzahlen/images/home/qr-rechnung-de.png" width="400"/>](https://www.einfach-zahlen.ch/content/dam/einfachzahlen/images/home/qr-rechnung-de.png)

<br>

## Aufgabenstellung

**Use Case**

Sie bekommen die Aufgabe, für Kleingewerbler und 
Kleinhändler oder Handwerker direkt aus deren Kassensystem, 
solche Standard-Rechnungen erstellen zu lassen. Die Kassen-
Systeme erzeugen Rohdaten, die Sie umformen und eine solche 
(Input-)Datei zu erstellen. In dieser Aufgabenstellung bekommen
Sie eine eigendefinierte Rohdatei in einem "csv"-ähnlichen Format
(csv = comma separated values).

**Input**

Sie bekommen dafür eine Serie von solchen Dateien:
<br>[x-ressourcen/rechnung23003.data](x-ressourcen/rechnung23003.data)
<br>[x-ressourcen/rechnung23004.data](x-ressourcen/rechnung23004.data)


**Output**

Mit solchen Daten können Sie über einen Upload korrekte
schweizer Rechnungen, wie sie seit 2022 Standard
sind, erstellen.

- [https://qr-rechnung.net/#/table](https://qr-rechnung.net/#/table)
- siehe auch 
<br>[x-ressourcen/beispiel-input-six.csv](x-ressourcen/beispiel-input-six.csv) (downloaded)
<br>[x-ressourcen/beispiel-input-six.xlsx](x-ressourcen/beispiel-input-six.xlsx) (selbst erzeugt)
<br>[x-ressourcen/QR-Rechnungen.xlsx](x-ressourcen/QR-Rechnungen.xlsx) (downloaded)



## Resultat / Abgabe

- Stufe 1
Für die Abgabe bekommen Sie von der Lehrperson drei neue .data-Dateien. 

- Stufe 2
Diese sollen dann "in einem Zug/in einem Durchgang" zu einer Tabelle verarbeitet werden.

- Stufe 3
Vor den Augen der Lehrperson lassen Sie dann hier 
[**https://qr-rechnung.net/#/table**](https://qr-rechnung.net/#/table)
eine Serie von QR-Rechnungen erstellen.


Bewertet wird dann der Download der QR-Rechnungen (wenns funktioniert). Je perfekter die heruntergeladenen
QR-Rechnungen aussehen, desto besser die Note.

- Stufe 4

FTP-Zugang

    HOST: "ftp.haraldmueller.ch
    USER: "schoolerinvoices
    PASS: "Berufsschule8005!
    PATH: "/out/[KlasseUndIhrNachname]"



## Bewertung 

| Stufe | Beschreibung | Punkte |
|-------|--------------|--------|
|     1 | Input-Daten können gelesen und interpretiert werden                           |  2 |
|     2 | Output-Liste kann erzeugt werden (Daten umformen)                             |  2 |
|     3 | Output-Liste kann QR-Rechnungen auf Web-Site erzeugen und Resultate "stimmen" |  2 |
|     4 | (Mehrere) Input-Dateien werden vom FTP-Drive abgeholt und verarbeitet         |  2 |
| Total |                                                                              | **8**|


## Benotung

| Note| Punkte    | Beschreibung |
|-----|-----------|--------------|
| 6.0 | über 7.7  | "perfekt" und Erwartungen und erweiterte Anforderungen klar übertroffen | 
| 5.5 |  6.8-7.6  | "sehr gut" und erweiterte Anforderungen übertroffen   |
| 5.0 |  6.0-6.8  | "gut", keine Nacharbeiten notwendig |
| 4.5 |  5.2-6.0  | einige Kleinigkeiten stimmen nicht, Nacharbeiten sind/wären notwendig, Lernziele sind weitgehend erreicht |
| 4.0 |  4.4-5.2  | "genügend", Lernziele erreicht, die Grundanforderungen erfüllt. Es ist häufig Unterstützung nötig. |
| 1.0 |Nichtabgabe|  |



