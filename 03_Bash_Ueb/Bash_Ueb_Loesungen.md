# Bash\Übung 1 - Beispiellösungen

[TOC]

## Übung 1 - Repetition: Navigieren in Verzeichnissen

1.  `[user@host: XY ]$ cd ~ `
2.  `[user@host: ~ ]$ cd /var/log`
3.  `[user@host: /var/log ]$ cd /etc/udev`
4.  `[user@host: /etc/udev ]$ cd ..`
5.  `[user@host: /etc/ ]$ cd network`
6.  `[user@host: /etc/network ]$ cd ../../dev`


## Übung 2 - Wildcards:

1.  `mkdir ~/Docs`
2.  `touch ~/Docs/file{1..10}`
3.  `rm -f ~/Docs/file1*`
4.  `rm -f ~/Docs/file[247]`
5.  `rm -f ~/Docs/* oder rm -f ~/Docs/file*`
6.  `mkdir Files`, `cd Files`
7.  `touch file{1..10}` (Erzeugt fileX von 1 bis 10)
8.  `cd ..` , `cp -R Files Files2`
9.  `cp -R Files Files2/Files3`
10.  `mv Files Files1`
11.  `rm -f *`


## Übung 3 - Tilde expansions:
> -   Siehe Theorie


## Übung 4 - grep, cut (, awk):

**a)**

```bash
cat file.txt | grep obelix
cat file.txt | grep 2
cat file.txt | grep e
cat file.txt | grep -v gamma
cat file.txt | grep -E "1|2|3"
```

**b)**

```bash
cat file.txt | cut -d ':' -f 1
cat file.txt | cut -d ':' -f 2
cat file.txt | cut -d ':' -f 3
```

**c)** 

```bash
awk -F ":" '{print $(NF-1)}' file.txt
# oder
cat file.txt | awk -F ":" '{print $(NF-1)}'
```



## Übung 5 - Für Fortgeschrittene:

-   Findet alle Zeilen, welche eine PCI-Adresse beinhalten
-   Findet IP-Adressen der Netzwerk-Anschlüsse


## Übung 6 - stdout, stdin, stderr:

**a)**
```bash
cat << END > datei.txt
a
b
c
d
e
END
```

**b)**
```bash
ls -z 2> errorsLs.log
```

**c)**

```bash
echo "sdfonsdodsf" > datei.txt
cat datei.txt > datei2.txt
cat datei.txt > datei2.txt
cat datei2.txt
cat datei.txt >> datei2.txt
cat datei.txt >> datei2.txt
cat datei2.txt
```

   Unterschied: `>>` hängt Inhalt an, `>` überschreibt Inhalt
	  
```bash
`cat datei.txt >> datei.txt`
```

   Erzeugt Fehler, Quell- ist Zieldatei

**d)**
`whoami > info.txt`

**e)**
`id >> info.txt`

**f)**
`cat info-txt | wc -w`




