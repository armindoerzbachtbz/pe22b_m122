# Übungen zu den Grundlagen

[TOC]

## Übung 1 - Repetition: Navigieren in Verzeichnissen

[Link zu Übung 1](Uebung_1.md)

## Übung 2 - Wildcards:

[Link zu Übung 2](Uebung_2.md)
## Übung 3 - Tilde expansions:

[Link zu Übung 3](Uebung_3.md)
## Übung 4 - grep, cut (, awk)::

[Link zu Übung 4](Uebung_4.md)
## Übung 5 - Für Fortgeschrittene:

[Link zu Übung 5](Uebung_5.md)
## Übung 6 - stdout, stdin, stderr:

[Link zu Übung 6](Uebung_6.md)
