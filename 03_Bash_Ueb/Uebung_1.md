## Übung 1 - Repetition: Navigieren in Verzeichnissen

1. Wechseln Sie mit `cd ` in Ihr Heimverzeichnis
2. Wechseln Sie ins Verzeichnis `/var/log` mit einer absoluten Pfadangabe
3. Wechseln Sie ins Verzeichnis `/etc/udev` mit einer absoluten Pfadangabe
4. Wechseln Sie ins Verzeichnis `/etc/` mit einer relativen Pfadangabe
5. Wechseln Sie ins Verzeichnis `/etc/network/` mit einer relativen Pfadangabe
6. Wechseln Sie ins Verzeichnis `/dev/` mit einer relativen Pfadangabe

