
## Übung 2 - Wildcards:

Lösen Sie folgende Übungen der Reihe nach (Verwenden Sie soviele Wildcards 
und/oder Braces wie nur irgendwie möglich! ):

1. Erzeugen Sie ein Verzeichnis Docs in ihrem Heimverzeichnis
2. Erstellen Sie die Dateien file1 bis file10 mit touch im Docs Verzeichnis
3. Löschen Sie alle Dateien, welche einer 1 im Dateinamen haben 
4. Löschen Sie file2, file4, file7 mit einem Befehl
5. Löschen Sie alle restlichen Dateien auf einmal 
6. Erzeugen Sie ein Verzeichnis Files in ihrem Heimverzeichnis
7. Erstellen Sie die Dateien file1 bis file10 mit touch im Files Verzeichnis
8. Kopieren Sie das Verzeichnis Files mitsamt Inhalt nach Files2
9. Kopieren Sie das Verzeichnis Files mitsamt Inhalt nach Files2/Files3
10. Benennen Sie das Verzeichnis Files in Files1 um
11. Löschen Sie alle erstellten Verzeichnisse und Dateien wieder
