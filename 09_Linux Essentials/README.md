![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m122_picto.png)

[TOC]

# M122 - Abläufe mit einer Scriptsprache automatisieren

---

### Lernziel:
* LZ 1

--- 

## Titel


### Video:
![Video](./x_gitressourcen/Video.png) 48min [![Konzept_Video](./x_gitressourcen/Konzept_Video.png)](https://web.microsoftstream.com/video/32dfb9f8-733b-454d-8932-70a73cc67a8a?channelId=4545eb7b-fbee-423c-9cab-38dade3ff53c)

### Bild:
![Bauplan_Haus](./x_gitressourcen/Bauplan_Haus.jpg)



---

# Checkpoint
* Offene Fragen?
