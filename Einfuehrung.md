## Einführung

Das Modul behandelt die Automatisierung von System-Abläufen mittels einer Skriptsprache. Diese muss von einem Interpreter (den sog. Scripting-**Host**) ausgeführt werden. 
Es gibt verschiedene [Skriptsprachen](https://de.wikipedia.org/wiki/Skriptsprache) in folgenden vier Anwendungsbereichen:

![](./x_gitressourcen/Anwendungsbereiche.png)

| Anwendungsbereich |  Skriptsprache (Interpreter = Scripting-"Host") z.Bsp.| Zweck der Automation (bzw. Fokus der Scriptsprache) |
| --- |:--- | --- |
| **Betriebssystem** |  **Bash (Unix, Linux, macOS)** <br> Batch (DOS, WINDOWS CMD) <br> Powershell (WINDOWS, Linux) <br> AppleScript (macOS) <br> Python| Scripts für Login, Installation, Konfiguration, usw. für das Betriebssystem oder dessen Nutzer(-accounts) |
| **Applikation** | VBA (MS Office, Libre Office) <br> LUA (Für eigene App)<br> AppleScript (mac-Apps) <br> UnityScript (Unity) | Ausführen von Abläufen innerhalb der Applikation |
| **WEB-Client** | JavaScript, TypeScript, JScript, Dart, ... (Interpreter im Browser) <br> JavaApplet (Java VM) | Die Scripts machen die Webseite dynamisch |
| **WEB-Server** |  PHP, NodeJS, Perl (Interpreter im Server) <br> JavaServlet (Java VM) <br> Python| Über die Scripts werden die Anfragen abgearbeitet. Die Logik der Web-Anwendungen wird damit aufgerufen oder ausgeführt. |

### Kommandozeileninterpreter

Manche Skriptsprachen sind von den Kommandozeileninterpretern der Betriebssysteme  abgeleitet. Die Interpreter sind vorrangig für interaktive Benutzung, d.h. für die Eingabe von Kommandos, ausgelegt. 

![](./x_gitressourcen/CompilerInterpreter.png)

*(Good to know: Bei einem Interpreter wird der Programmtext Zeile für Zeile abgearbeitet, wobei er das Programm auf Fehler überprüft. Jeder gefundene Fehler führt bei einem Interpreter zum Abbruch der Übersetzung; die fehlerhafte Zeile wird angezeigt und kann berichtigt werden. Anschließend kann der Übersetzungsprozess neu gestartet werden. Leider wird die Fehlerprüfung auch dann beibehalten, wenn das Programm fehlerfrei und lauffähig ist. Wieder wird der Programmtext Zeile für Zeile in Maschinencode übersetzt und ausgeführt. Da die Fehlerüberprüfung recht viel Zeit erfordert, sind durch einen Interpreter übersetzte Programme relativ langsam und dadurch für komplexe Lösungen untauglich >> Hochsprachen mit Compiler.)*

Die Eingabesprache wird um Variablen, arithmetische Ausdrücke, Kontrollstrukturen (if, while) und anderes erweitert und ermöglicht so die Automatisierung von Aufgaben (z. B. bei der unbeaufsichtigten Installation), indem „kleine Programme“ in Dateien geschrieben werden. Diese Dateien können dann vom Interpreter ausgeführt werden. Die Dateien nennt man unter dem Betriebssystem Unix/Linux **Shellskripte** (ausgeführt von einer der Unix-Shells bash, csh …) oder unter DOS und Windows auch Stapelverarbeitungsdateien oder Batch-Skripte (Ausgeführt von cmd.exe, powershell.exe).  (Wikipedia) 

In diesem Modul beginnen wir mit **Bash auf einem Linux-System** ...
